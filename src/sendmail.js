
var nodemailer = require('nodemailer');
var markdown = require('nodemailer-markdown').markdown;
var fs = require('fs');

/////////////////////////////////////////////////////
// Generate Mail

// Read Original File
var contents = fs.readFileSync(__dirname + '/../daily.md', 'utf-8');
var lines = contents.split('\n');

// Subject
var subject = lines[0];

// Output buffer
var outbuff = [];

// Worktime calc
var worktimeLineNum = 65535;
var worktimeArray = [];
var worktimeArrayIndex = 0;

for (var i=1 ; i<lines.length ; i++){
  //worktime検出フラグ
  if (lines[i].match(/■出社時間\/退社時間/)) {
    worktimeLineNum = i + 2;
  }
  //worktime配列
  if (i > worktimeLineNum) {
    if (lines[i].match(/\d{2}\:\d{2}-\d{2}\:\d{2}/)) {
      worktimeArray[worktimeArrayIndex] = lines[i];
      worktimeArrayIndex++;
    }
  }
  //出力バッファへ格納
  outbuff.push(lines[i]);
}

var workStartLine = worktimeArray.shift().split("|")[1].trim(); //配列の先頭を|で分割した2番目の要素の空白を取り除く
var workFinishLine = worktimeArray.pop().split("|")[1].trim(); //配列の最後を|で分割した2番目の要素の空白を取り除く
var workStartTime = workStartLine.match(/^\d{2}\:\d{2}/);
var workFinishTime = workFinishLine.match(/\d{2}\:\d{2}$/);
var worktime = "* " + workStartTime + " - " + workFinishTime
console.log('start  = ' + workStartTime);
console.log('finish = ' + workFinishTime);
console.log('finish = ' + worktime);

outbuff[worktimeLineNum-1] = worktime;

//for (var i=0 ; i<outbuff.length ; i++){
//  console.log(outbuff[i]);
//}

var outstr = outbuff.join("\n");
console.log(outstr);

// TODO: (Mid) subject
// subjectのvalidation (git hookかな)

// TODO: (Low) 問題項目からUpdateにCheck

// TODO: (High) 作業内容から作業割合を算出
// 10:00-13:00 [部内Mtg] あれこれ [03:00]
// 13:00-23:30 [CoolFire] それこれ [10:30]


// create reusable transporter object using the default SMTP transport
var smtpConfig = {
    host: 'smtp.gmail.com',
    port: 465,
    secure: true, // use SSL
    auth: {
        user: 'yo_kudo@medialinks.co.jp',
        pass: 'c7b07437'
    }
};

var transporter = nodemailer.createTransport(smtpConfig);

// setup e-mail data with unicode symbols
var mailOptions = {
    from: '"Yoji Kudo @ MGL" <yo_kudo@medialinks.co.jp>', // sender address
    //to: '"Yoji Kudo @ MGL" <yo_kudo@medialinks.co.jp>', // list of receivers
    to: '"dev_daily" <dev_daily@medialinks.co.jp>', // list of receivers
    subject: subject, // Subject line
    //markdown: {path: __dirname + '/../daily.md'}
    markdown: outstr
    //markdown: '# Hello world!\n\nThis is a **markdown** message'
};

// send mail with defined transport object
transporter.use('compile', markdown());
transporter.sendMail(mailOptions, function(error, info){
    if (error) {
        return console.log(error);
    }
    console.log('Message sent: ' + info.response);
});


